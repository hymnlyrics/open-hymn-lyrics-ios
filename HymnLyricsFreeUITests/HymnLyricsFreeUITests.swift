//
//  HymnLyricsFreeUITests.swift
//  HymnLyricsFreeUITests
//
//  Created by Girish on 05/01/19.
//  Copyright © 2019 Ecodia. All rights reserved.
//

import XCTest

class HymnLyricsFreeUITests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func test_a_search() {
        let txtSearch = app.textFields["Jump to"]
        txtSearch.tap()
        txtSearch.typeText("to")
        app.toolbars["Toolbar"].buttons["Toolbar Done Button"].tap()
        test_b_filter()
    }
    
    func test_b_filter() {
        let element = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element
        let button = element.children(matching: .other).element(boundBy: 0).children(matching: .other).element(boundBy: 0).children(matching: .button).element
        button.tap()
        app.buttons["Show Favorites"].tap()
        
        button.tap()
        app.buttons["Filter By Title"].tap()
        let scrollViewsQuery = app.scrollViews
        let textField = scrollViewsQuery.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .textField).element
        textField.tap()
        textField.typeText("title")
        let toolbarDoneButtonButton = app.toolbars["Toolbar"].buttons["Toolbar Done Button"]
        toolbarDoneButtonButton.tap()
        let filterButton = scrollViewsQuery.otherElements.buttons["FILTER"]
        filterButton.tap()
        
        button.tap()
        app.buttons["Filter By Lyrics"].tap()
        textField.tap()
        textField.typeText("kingdom")
        toolbarDoneButtonButton.tap()
        filterButton.tap()
        
        button.tap()
        app.buttons["Filter By Category"].tap()
        app.tables.staticTexts["Children"].tap()
        button.tap()
        app.buttons["Show All"].tap()
    }
    
    func test_c_settings() {
        let element2 = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element
        let button2 = element2.children(matching: .other).element(boundBy: 0).children(matching: .other).element(boundBy: 1).children(matching: .button).element
        button2.tap()
        
        let element = element2.children(matching: .other).element(boundBy: 1).children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element
        let button = element.children(matching: .other).element(boundBy: 0).children(matching: .button).element
        button.tap()
        let okButton = app.buttons["OK"]
        okButton.tap()
        
        button2.tap()
        let button3 = element.children(matching: .other).element(boundBy: 1).children(matching: .button).element
        button3.tap()
        let scrollViewsQuery = app.scrollViews
        let element3 = scrollViewsQuery.children(matching: .other).element.children(matching: .other).element
        element3.children(matching: .other).element(boundBy: 0).children(matching: .other).element.children(matching: .button).element.tap()
        app.tables.staticTexts["Blue/White"].tap()
        element3.children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .button).element.tap()
        app.tables.staticTexts["XS"].tap()
        element3.children(matching: .other).element(boundBy: 2).children(matching: .other).element.children(matching: .button).element.tap()
        let textField = element3.children(matching: .other).element(boundBy: 3).children(matching: .textField).element
        textField.tap()
        textField.typeText("hymn")
        app.toolbars["Toolbar"].buttons["Toolbar Done Button"].tap()
        scrollViewsQuery.otherElements.buttons["RESET"].tap()
        app.buttons["OK"].tap()
    }
    
}
