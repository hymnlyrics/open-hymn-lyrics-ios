/**
 * Copyright (c) 2018 Ecodia
 */
import UIKit
import SQLite3
import MessageUI
import MobileCoreServices

class ViewController: UIViewController,UIGestureRecognizerDelegate, UIDocumentPickerDelegate,UIDocumentInteractionControllerDelegate {
    
    
    //MARK:- IBOutlet Add Custom Songs
    @IBOutlet var viewMain_InsertCustomSong: UIView!
    @IBOutlet weak var txtTitle_InsertCustomSong: UITextField!
    @IBOutlet weak var txtAuthor_InsertCustomSong: UITextField!
    @IBOutlet weak var txtNumber_InsertCustomSon: UITextField!
    @IBOutlet weak var txtViewLyrics_InsertCustomSong: UITextView!
    
    
    //MARK:- IBOutlet Custom Songs
    @IBOutlet var viewMain_CustomSong: UIView!
    
    //MARK:- IBOutlet Edit Song Number
    @IBOutlet var viewMain_EditSongNumber: UIView!
    @IBOutlet weak var txt_EditSongNumber: UITextField!
    
    //MARK:- IBOutlet resetConfirmation
    @IBOutlet var viewMain_resetConfirmation: UIView!
    @IBOutlet var viewMain_Initilizingdata_resetConfirmation: UIView!
    @IBOutlet weak var progresssView: UIProgressView!
    @IBOutlet weak var lblCount_resetConfirmation: UILabel!
    
    //MARK:- IBOutlet SwiftyVerticalScrollBar Declare
    fileprivate (set) var scrollBar : SwiftyVerticalScrollBar!
    
    //MARK:- IBOutlet ScrollTimeDisplay
    @IBOutlet var viewMian_ScrollTimeDisplay: UIView!
    @IBOutlet weak var lbltitle_ScrollTimeDisplay: UILabel!
    
    
    //MARK:- IBOutlet Category Filter Popup
    @IBOutlet var viewMain_CategoryFilter: UIView!
    @IBOutlet weak var tblList_CategoryFilter: UITableView!
    
    //
    @IBOutlet weak var viewMainBg_Color: UIView!
    //MARK:- IBOutlet Jump to
    @IBOutlet weak var txtJumpTo: UITextField!
    
    
    //MARK:- IBOutlet LongPress Popup
    @IBOutlet var viewMain_LongPress: UIView!
    @IBOutlet weak var lblTitle_LongPress: UILabel!
    @IBOutlet weak var btnFavorites_LongPress: UIButton!
    @IBOutlet weak var viewEditSong_height_LongPress: NSLayoutConstraint!//40
    @IBOutlet weak var viewEditSongNumber_LongPress: UIView!
    @IBOutlet weak var viewEdit_CustomSong_LongPress: UIView!
    @IBOutlet weak var viewEdit_CusomSong_Height_LongPress: NSLayoutConstraint!//40
    @IBOutlet weak var viewDelete_CustomSong_LongPress: UIView!
    @IBOutlet weak var viewDelete_CusomSong_Height_Longpress: NSLayoutConstraint!//40
    
    
    //MARK:- IBOutlet Filter Popup
    @IBOutlet var viewMain_TitleFilter: UIView!
    @IBOutlet weak var txtTitle_TitleFilter: UITextField!
    @IBOutlet var viewMain_LyricsFilter: UIView!
    @IBOutlet weak var txtlyrics_LyricsFilter: UITextField!
    @IBOutlet var viewMain_Filter: UIView!
    @IBOutlet weak var btnByTitle_Filter: UIButton!
    @IBOutlet weak var viewCustomSong_Filter: UIView!
    @IBOutlet weak var viewBuiltin_Filter: UIView!
    @IBOutlet weak var viewCustomsong_height_Filter: NSLayoutConstraint!//40
    @IBOutlet weak var viewBuiltIn_Height_Filter: NSLayoutConstraint!//40
    
    //---
    @IBOutlet weak var lblSelectFilterTitle: UILabel!
    @IBOutlet weak var lblAppname_About: UILabel!
    @IBOutlet weak var lblAppName_Header: UILabel!
    
    
    //-----
    internal let SQLITE_STATIC = unsafeBitCast(0, to: sqlite3_destructor_type.self)
    internal let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)
    //----
    
    //
    var arrListSection:NSMutableArray = NSMutableArray()
    //var arrListSectionMain:NSMutableArray = NSMutableArray()
    
    var arrListRow:NSMutableArray = NSMutableArray()
    var selectSectionIndex:Int = -1
    
    @IBOutlet weak var tblList: UITableView!
    
    //MARK:- IBOutlet Menu
    @IBOutlet weak var viewMainPopup: UIView!
    @IBOutlet var viewMenu: UIView!
    @IBOutlet weak var viewCustomSong_Height_menu: NSLayoutConstraint!//40
    @IBOutlet weak var viewCustomSong_Menu: UIView!
    @IBOutlet weak var viewHymnLyricsApps_Menu1: UIView!
    @IBOutlet weak var viewTranslations_Height: NSLayoutConstraint!
    
    
    //MARK:- IBOutlet About
    @IBOutlet var viewMain_About: UIView!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var lblTotalSong: UILabel!
    @IBOutlet weak var lblDisplaySong: UILabel!
    @IBOutlet weak var btnFeedbackLink: UIButton!
    
    
    //MARK:- IBOutlet Setting menu
    @IBOutlet var viewSettingMenu: UIView!
    @IBOutlet weak var lblStyle_SettingMenu: UILabel!
    @IBOutlet weak var btnStyle_SettingMenu: UIButton!
    @IBOutlet weak var lblFontSize_SettingMenu: UILabel!
    @IBOutlet weak var btnFontSize_SettingMenu: UIButton!
    @IBOutlet weak var imgCheckOpenNewScreen_SettingMenu: UIImageView!
    @IBOutlet weak var btnNewScreen_SettingMenu: UIButton!
    @IBOutlet weak var btnOk_SettingMenu: UIButton!
    @IBOutlet weak var txtYoutubeSearchWord_settingMenu: UITextField!
    @IBOutlet weak var imgCheck_ShowHymnNumber_SettingMenu: UIImageView!
    @IBOutlet weak var btnShowHymnNumber_SettingMenu: UIButton!
    @IBOutlet weak var viewEditSong_SettingMenu_Height: NSLayoutConstraint!//35
    @IBOutlet weak var viewShowHymnNumber_SettingMenu: UIView!
    
    
    //--
    var tblfontSize:Int = 17
    var openNewScreenForDetail:Int = 0
    var selectColour = String()
    
    var selectLongPressDic = NSDictionary()
    var selectLongpreeType = String()
    var selectJumpIndex:Int = 1
    
    var isDisplayCenterOfFirstCharacter:Bool = false
    var arrlist_categoryFilter: NSMutableArray = NSMutableArray()
    
    var isUpdateCustomSong:String = "0"
    var selectfileURL: URL!

    var longPressRecognizer = UILongPressGestureRecognizer()
    var longPressRecognizer_Row = UILongPressGestureRecognizer()
    
    let KeyArry = ["Title: ","Author: ","Keywords: ","Tune: ","LastUpdate: ","Background: ","Lyrics:"]
    let KeyOFMaster = ["Title","Author","Keywords","Tune","LastUpdate","Background","Lyrics"]
    
    var isInitDB = false
    
    var dataFileContents : [[String:String]] = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commonData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if (loadSongDataFile()) {
            if (checkIfDatabaseNeedsUpdating()) {
                viewMain_Initilizingdata_resetConfirmation.frame = viewMainPopup.frame
                viewMainPopup.addSubview(viewMain_Initilizingdata_resetConfirmation)
                DispatchQueue.global().async {
                    self.insertSongData(dataArray: self.dataFileContents, custom: "0")
                    DispatchQueue.main.sync {
                        self.selectSongData()
                        self.viewMainPopup.isHidden = true
                        self.removeOtherViewToMainPopup()
                    }
                }
            } else {
                selectSongData()
            }
        } else {
            selectSongData()
        }
    }
    
    func loadSongDataFile() -> Bool {
        if let path = Bundle.main.path(forResource: "SongDataFile", ofType: "txt") {
            do {
                let text = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
                
                dataFileContents = loadSongDataFile(text)
                
                for (_,data) in dataFileContents.enumerated() {
                    let strcat = data["Keywords"]!
                    let arrCateTemp = strcat.components(separatedBy: ",")
                    if arrCateTemp.count != 0
                    {
                        if !arrlist_categoryFilter.contains(arrCateTemp[0])
                        {
                            if arrCateTemp[0] != ""
                            {
                                arrlist_categoryFilter.add(arrCateTemp[0])
                            }
                        }
                    }
                }
                let swiftArray = arrlist_categoryFilter as AnyObject as! [String]
                arrlist_categoryFilter = NSMutableArray(array: swiftArray.sorted(by: <))
            } catch {
                print("Failed to read text from SongDataFile")
                return false
            }
            return true
        }
        else {
            print("Could not load SongDataFile")
            return false
        }
    }
    
    func checkIfDatabaseNeedsUpdating() -> Bool {
        
        let songDataCount = dataFileContents.count
        let databaseDataCount = Database.countTblData(builtinOnly: true)
        
        if (songDataCount != databaseDataCount) {
            return true
        }
        else {
            return false
        }
    }
        
    func loadSongDataFile(_ text : String) -> [[String:String]] {
        
        var dataArray : [[String:String]] = [[String:String]]()

        let arrayLyrcs = text.components(separatedBy: "---")

        if arrayLyrcs.count != 0 {
            
            var temArry = [[String:String]]()
            
            for lyrcs in arrayLyrcs {
                
                var valueArry : [String:String] = [String:String]()
                
                let arrObj = lyrcs.components(separatedBy: "\n")
                
                if arrObj.count != 0 {
                    
                    var lyricsStart = false
                    
                    for obj in arrObj {
                        
                        if obj.contains(KeyArry[0]) {
                            let title = obj.getValueOf(KeyArry[0])
                            valueArry[KeyOFMaster[0]] = title
                        }else if obj.contains(KeyArry[1]) {
                            let title = obj.getValueOf(KeyArry[1])
                            valueArry[KeyOFMaster[1]] = title
                        }else if obj.contains(KeyArry[2]) {
                            let title = obj.getValueOf(KeyArry[2])
                            valueArry[KeyOFMaster[2]] = title
                        }else if obj.contains(KeyArry[3]) {
                            let title = obj.getValueOf(KeyArry[3])
                            valueArry[KeyOFMaster[3]] = title
                        }else if obj.contains(KeyArry[4]) {
                            let title = obj.getValueOf(KeyArry[4])
                            valueArry[KeyOFMaster[4]] = title
                        }else if obj.contains(KeyArry[5]) {
                            let title = obj.getValueOf(KeyArry[5])
                            valueArry[KeyOFMaster[5]] = title
                        }else if obj.contains(KeyArry[6]) {
                            lyricsStart = true
                        }
                        else {
                            if lyricsStart {
                                if let valueLyrics = valueArry[KeyOFMaster[6]] {
                                    valueArry[KeyOFMaster[6]] = "\(valueLyrics)\n\(obj)"
                                }
                                else{
                                    valueArry[KeyOFMaster[6]] = obj
                                }
                            }
                        }
                    }
                }
                
                temArry.append(valueArry)
            }
            
            for data in temArry {
                var newData = [String:String]()
                var isNotBank = false
                for keys in KeyOFMaster {
                    if var dataValue : String = data[keys] {
                        isNotBank = true
                        newData[keys] = dataValue.trim()
                    }else{
                        newData[keys] = ""
                    }
                }
                if isNotBank {
                    dataArray.append(newData)
                }
            }
        } else {
            print("no Data found ---")
        }
        
        return dataArray
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func insertSongData(dataArray:[[String:String]], custom:String)
    {
        var dbStatus = DBStatus()
        let size = Float(dataArray.count)
        for (index,data) in dataArray.enumerated() {
            let title = data[self.KeyOFMaster[0]]!
            let id = Database.getIdOfSongTitle(title: title)
            
            if (id == -1) {
                print("Inserting \(title)")
                
                let lyrics = data[self.KeyOFMaster[6]]!
                let tune = data[self.KeyOFMaster[3]]!
                let lastUpdate = data[self.KeyOFMaster[4]]!
                let author = data[self.KeyOFMaster[1]]!
                let keywords = data[self.KeyOFMaster[2]]!
                let background = data[self.KeyOFMaster[5]]!
                
                Database.insertDataTbl(title: title, lyrics: lyrics, tune: tune, lastUpdate: lastUpdate, author: author, keyword: keywords, background: background, hymn_number: DB.MAX_INT, customsong: custom)
                
                dbStatus.inserted += 1
            }
            else {
                dbStatus.skipped += 1
            }
            
            DispatchQueue.main.async { () -> Void in
                let ratio = Float(index) / size
                self.progresssView.setProgress(ratio, animated: false)
            }
        }
    }
    
    func selectSongData() {
        loadFromDatabase()
        setFontStyleAndSize(label: UILabel(), img: UIImageView(), isFav: "", isReloadtbl: "1")
    }
    
    func loadFromDatabase() {
        arrListSection = Database.retrieveHymnData(showHymnNumber: Common.is_ShowHymnNumber())
    }
    
    @objc func handleTap(_ recognizer: UITapGestureRecognizer) {
        // Perform operation
    }
    
    func commonData()  {
        //--
        if Bundle.main.bundleIdentifier == Common.hymnlyricsfree_identifire
        {
            viewEditSong_height_LongPress.constant = 0
            viewEditSong_SettingMenu_Height.constant = 0
            viewEditSongNumber_LongPress.isHidden = true
            viewShowHymnNumber_SettingMenu.isHidden = true
            
            viewCustomSong_Height_menu.constant = 0
            viewCustomSong_Menu.isHidden = true
            viewBuiltIn_Height_Filter.constant = 0
            viewBuiltin_Filter.isHidden = true
            viewCustomsong_height_Filter.constant = 0
            viewCustomSong_Filter.isHidden = true
            
            hiddenEditDeleteCustomSongView()
        }
        else if Bundle.main.bundleIdentifier == Common.hymnlyricsplus_identifire
        {
            viewEditSong_height_LongPress.constant = 40
            viewEditSong_SettingMenu_Height.constant = 35
            viewEditSongNumber_LongPress.isHidden = false
            viewShowHymnNumber_SettingMenu.isHidden = false
            
            viewCustomSong_Height_menu.constant = 0
            viewCustomSong_Menu.isHidden = true
            viewBuiltIn_Height_Filter.constant = 0
            viewBuiltin_Filter.isHidden = true
            viewCustomsong_height_Filter.constant = 0
            viewCustomSong_Filter.isHidden = true
            
            hiddenEditDeleteCustomSongView()
        }
        else if Bundle.main.bundleIdentifier == Common.hymnlyricsplusads_identifire
        {
            viewEditSong_height_LongPress.constant = 40
            viewEditSong_SettingMenu_Height.constant = 35
            viewEditSongNumber_LongPress.isHidden = false
            viewShowHymnNumber_SettingMenu.isHidden = false
        
            viewCustomSong_Height_menu.constant = 40
            viewCustomSong_Menu.isHidden = false
            viewBuiltIn_Height_Filter.constant = 40
            viewBuiltin_Filter.isHidden = false
            viewCustomsong_height_Filter.constant = 40
            viewCustomSong_Filter.isHidden = false
            
            showEditDeleteCustomSongView()
        }
        
        //--        
        lblAppname_About.text = (Bundle.main.infoDictionary!["CFBundleName"] as! String)
        lblAppName_Header.text = (Bundle.main.infoDictionary!["CFBundleName"] as! String)
        
        //--
        if (UserDefaults.standard.object(forKey: "fontSize") == nil) {
            UserDefaults.standard.set("M", forKey: "fontSize")
        }
        
        //--
        let appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
        lblVersion.text = "Version: " + appVersion!
        
        if (UserDefaults.standard.object(forKey: "openNewScreen") == nil) {
            UserDefaults.standard.set(openNewScreenForDetail, forKey: "openNewScreen")
        }
        else {
            openNewScreenForDetail = UserDefaults.standard.object(forKey: "openNewScreen") as! Int
        }
        
        //--
        self.scrollBar = SwiftyVerticalScrollBar(frame: CGRect.zero, targetScrollView: self.tblList)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.delegate = self
        scrollBar.addGestureRecognizer(tap)
        self.viewMainBg_Color.addSubview(self.scrollBar!)
        self.scrollBar.scrollStart = {
            self.removeOtherViewToMainPopup()
            self.viewMian_ScrollTimeDisplay.frame = self.viewMainPopup.frame
            self.viewMainPopup.addSubview(self.viewMian_ScrollTimeDisplay)
            self.viewMainPopup.isHidden = false
        }
        self.scrollBar.scrollEnd = {
            self.viewMainPopup.isHidden = true
        }
        
        //--
        txtJumpTo.placeholderColor = UIColor.white
        
        //--
        longPressRecognizer_Row = UILongPressGestureRecognizer(target: self, action: #selector(longPressed_row(sender:)))
        longPressRecognizer_Row.minimumPressDuration = 1.0
        longPressRecognizer_Row.delegate = self
        tblList.addGestureRecognizer(longPressRecognizer_Row)
    }
    
    func hiddenEditDeleteCustomSongView()  {
        viewEdit_CustomSong_LongPress.isHidden = true
        viewEdit_CusomSong_Height_LongPress.constant = 0
        viewDelete_CustomSong_LongPress.isHidden = true
        viewDelete_CusomSong_Height_Longpress.constant = 0
    }
    func showEditDeleteCustomSongView()  {
        viewEdit_CustomSong_LongPress.isHidden = false
        viewEdit_CusomSong_Height_LongPress.constant = 40
        viewDelete_CustomSong_LongPress.isHidden = false
        viewDelete_CusomSong_Height_Longpress.constant = 40
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollBar.frame = CGRect(x: viewMainBg_Color.frame.size.width-40, y: viewMainBg_Color.frame.origin.y, width: 40, height: self.tblList.bounds.size.height)
    }
    
    func removeOtherViewToMainPopup()  {
        self.view.endEditing(true)
        viewMenu.removeFromSuperview()
        viewSettingMenu.removeFromSuperview()
        viewMain_Filter.removeFromSuperview()
        viewMain_TitleFilter.removeFromSuperview()
        viewMain_LongPress.removeFromSuperview()
        viewMain_LyricsFilter.removeFromSuperview()
        viewMian_ScrollTimeDisplay.removeFromSuperview()
        viewMain_CategoryFilter.removeFromSuperview()
        viewMain_About.removeFromSuperview()
        viewMain_resetConfirmation.removeFromSuperview()
        viewMain_Initilizingdata_resetConfirmation.removeFromSuperview()
        viewMain_EditSongNumber.removeFromSuperview()
        viewMain_CustomSong.removeFromSuperview()
        viewMain_InsertCustomSong.removeFromSuperview()
    }
    
    
    func sendEmail() {
        let appURL = URL(string: "mailto:otseng@ecodia.com")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(appURL as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }
        else {
            UIApplication.shared.openURL(appURL as URL)
        }
    }
    
    // MARK: - documentPicker delegate
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        _ = url.startAccessingSecurityScopedResource()
        let coordinator = NSFileCoordinator()
        
        var error:NSError? = nil
        coordinator.coordinate(readingItemAt: url, options: [], error: &error) { (url) -> Void in
            let fileData = NSData(contentsOf: url)
            var dataFileContents : [[String:String]] = [[String:String]]()
            dataFileContents = self.loadSongDataFile(String(data: fileData! as Data, encoding: String.Encoding.ascii)!)
            insertSongData(dataArray: dataFileContents, custom: "1")
            selectSongData()
        }
        url.stopAccessingSecurityScopedResource()
    }
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    func grabFileURL(_ fileName: String?) -> URL? {
        var documentsURL: URL? = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
        documentsURL = documentsURL?.appendingPathComponent(fileName ?? "")
        return documentsURL
    }
    
    //MARK:- Filter Func
    func filterSongList(type: String, otherText: String)  {
        if type == "showall"
        {
            clearFilter()
            selectSongData()
        }
        else if type == "title"
        {
            lblSelectFilterTitle.text = NSLocalizedString("Title", comment: "") + ": " + txtTitle_TitleFilter.text!
            arrListSection = Database.retrieveHymnData(filterByCol: DB.TITLE, filterByText: txtTitle_TitleFilter.text!,
                                                       showHymnNumber: Common.is_ShowHymnNumber())
        }
        else if type == "lyrics"
        {
            lblSelectFilterTitle.text = NSLocalizedString("Lyrics", comment: "") + ": " + txtlyrics_LyricsFilter.text!
            arrListSection = Database.retrieveHymnData(filterByCol: DB.LYRICS, filterByText: txtlyrics_LyricsFilter.text!,
                                                       showHymnNumber: Common.is_ShowHymnNumber())
        }
        else if type == "favorites"
        {
            lblSelectFilterTitle.text = NSLocalizedString("Favorites", comment: "")
            arrListSection = Database.retrieveHymnData(filterByCol: DB.FAVORITE,
                                                       showHymnNumber: Common.is_ShowHymnNumber())
        }
        else if type == "category"
        {
            lblSelectFilterTitle.text = NSLocalizedString("Category", comment: "") + ": " + otherText
            arrListSection = Database.retrieveHymnData(filterByCol: DB.KEYWORD, filterByText: otherText,
                                                       showHymnNumber: Common.is_ShowHymnNumber())
        }
        else if type == "customsong"
        {
            lblSelectFilterTitle.text = NSLocalizedString("Custom Songs", comment: "")
            arrListSection = Database.retrieveHymnData(filterByCol: DB.CUSTOM_SONG, filterByText: "1",
                                                       showHymnNumber: Common.is_ShowHymnNumber())
        }
        else if type == "builtin"
        {
            lblSelectFilterTitle.text = NSLocalizedString("Built-in Songs", comment: "")
            arrListSection = Database.retrieveHymnData(filterByCol: DB.CUSTOM_SONG, filterByText: "0",
                                                       showHymnNumber: Common.is_ShowHymnNumber())
        }
        tblList.reloadData()
    }
    
    //MARK:- Clear Filter Popup
    func clearFilter()  {
        lblSelectFilterTitle.text = NSLocalizedString("Show All", comment: "")
        txtlyrics_LyricsFilter.text = ""
        txtTitle_TitleFilter.text = ""
        selectSongData()
    }
    
    //MARK:- setFont Style and Size
    func setFontStyleAndSize(label: UILabel, img: UIImageView, isFav: String, isReloadtbl:String)  {
        
        //--
        if (UserDefaults.standard.object(forKey: "fontSize") != nil) {
            lblFontSize_SettingMenu.text = (UserDefaults.standard.object(forKey: "fontSize") as! String)
        }
        if (UserDefaults.standard.object(forKey: "fontStyle") != nil) {
            lblStyle_SettingMenu.text = (UserDefaults.standard.object(forKey: "fontStyle") as! String)
        }
        
        //--
        if lblFontSize_SettingMenu.text == NSLocalizedString("XS", comment: "")
        {
            tblfontSize = 10
        }
        else if lblFontSize_SettingMenu.text == NSLocalizedString("S", comment: "")
        {
            tblfontSize = 14
        }
        else if lblFontSize_SettingMenu.text == NSLocalizedString("M", comment: "")
        {
            tblfontSize = 18
        }
        else if lblFontSize_SettingMenu.text == NSLocalizedString("L", comment: "")
        {
            tblfontSize = 22
        }
        else if lblFontSize_SettingMenu.text == NSLocalizedString("XL", comment: "")
        {
            tblfontSize = 26
        }
        label.font = label.font.withSize(CGFloat(tblfontSize))
        
        let fgColor = Common.getForegroundColor(lblStyle_SettingMenu.text!)
        let bgColor = Common.getBackgroundColor(lblStyle_SettingMenu.text!)
        
        txtJumpTo.placeholderColor = fgColor
        txtJumpTo.textColor = fgColor
        lblSelectFilterTitle.textColor = fgColor
        label.textColor = fgColor
        if isFav == "1" {
            img.image = img.image!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
            img.tintColor = fgColor
        }
        viewMainBg_Color.backgroundColor = bgColor
        
        //---
        selectColour = lblStyle_SettingMenu.text!
        if isReloadtbl == "1"
        {
            self.tblList.reloadData()
        }
    }
    
    //MARK:- UIButton Action reset Confirmation
    @IBAction func btnCancel_resertConfirmation(_ sender: Any) {
        viewMainPopup.isHidden = true
    }
    @IBAction func btnOk_resetConfirmation(_ sender: Any) {
        viewMainPopup.isHidden = true
        removeOtherViewToMainPopup()
        viewMain_Initilizingdata_resetConfirmation.frame = viewMainPopup.frame
        viewMainPopup.addSubview(viewMain_Initilizingdata_resetConfirmation)
        viewMainPopup.isHidden = false
        progresssView.setProgress(0, animated: false)
        DispatchQueue.global().async {
            Database.resetDB()
            if (self.loadSongDataFile()) {
                self.insertSongData(dataArray: self.dataFileContents, custom: "0")
                self.loadFromDatabase()
                DispatchQueue.main.sync {
                    self.selectSongData()
                    self.viewMainPopup.isHidden = true
                    self.removeOtherViewToMainPopup()
                }
            }
        }
    }
    
    @IBAction func btnResetDatabase(_ sender: Any) {
        viewMainPopup.isHidden = true
        removeOtherViewToMainPopup()
        viewMain_resetConfirmation.frame = viewMainPopup.frame
        viewMainPopup.addSubview(viewMain_resetConfirmation)
        viewMainPopup.isHidden = false
    }
    
    //MARK:- UIButton Action ScrollTimeDisplay
    @IBAction func btnBack_ScrollTimeDisplay(_ sender: Any) {
        viewMainPopup.isHidden = true
    }
    
    //MARK:- UIButtonAction Filter Popup
    @IBAction func btnShowAll_Filter(_ sender: Any) {
        self.view.endEditing(true)
        filterSongList(type: "showall", otherText: "")
        viewMainPopup.isHidden = true
    }
    @IBAction func btnShowFavorites_Filter(_ sender: Any) {
        filterSongList(type: "favorites", otherText: "")
        viewMainPopup.isHidden = true
    }
    
    @IBAction func btnByTitle_Filter(_ sender: Any) {
        self.view.endEditing(true)
        viewMainPopup.isHidden = true
        removeOtherViewToMainPopup()
        viewMain_TitleFilter.frame = viewMainPopup.frame
        viewMainPopup.addSubview(viewMain_TitleFilter)
        viewMainPopup.isHidden = false
        
    }
    @IBAction func btnByLyrics_Filter(_ sender: Any) {
        self.view.endEditing(true)
        viewMainPopup.isHidden = true
        removeOtherViewToMainPopup()
        viewMain_LyricsFilter.frame = viewMainPopup.frame
        viewMainPopup.addSubview(viewMain_LyricsFilter)
        viewMainPopup.isHidden = false
    }
    
    @IBAction func btnByCategory_Filter(_ sender: Any) {
        self.view.endEditing(true)
        viewMainPopup.isHidden = true
        removeOtherViewToMainPopup()
        viewMain_CategoryFilter.frame = viewMainPopup.frame
        viewMainPopup.addSubview(viewMain_CategoryFilter)
        viewMainPopup.isHidden = false
    }
    @IBAction func btnShowCustomSong_Filter(_ sender: Any) {
        filterSongList(type: "customsong", otherText: "")
        viewMainPopup.isHidden = true
    }
    @IBAction func btnShowBuiltin_FIlter(_ sender: Any) {
        filterSongList(type: "builtin", otherText: "")
        viewMainPopup.isHidden = true
    }
    
    
    
    @IBAction func btnBack_Filter(_ sender: Any) {
        self.view.endEditing(true)
        viewMainPopup.isHidden = true
    }
    
    //MARK:- UIButton Action Category Filter Popup
    @IBAction func btnBack_CategoryFilter(_ sender: Any) {
        viewMainPopup.isHidden = true
    }
    
    //MARK:- UIButton Action Lyrics Filter Popup
    @IBAction func btnBack_LyricsFilter(_ sender: Any) {
        self.view.endEditing(true)
        viewMainPopup.isHidden = true
    }
    @IBAction func btnFilter_LyricsFilter(_ sender: Any) {
        self.view.endEditing(true)
        filterSongList(type: "lyrics", otherText: "")
        viewMainPopup.isHidden = true
    }
    @IBAction func btnClear_LyricsFilter(_ sender: Any) {
        self.view.endEditing(true)
        clearFilter()
        viewMainPopup.isHidden = true
    }
    
    //MARK:- UIButton Action Title Filter Popup
    @IBAction func btnBack_TitleFilter(_ sender: Any) {
        self.view.endEditing(true)
        viewMainPopup.isHidden = true
    }
    @IBAction func btnFilter_TitleFilter(_ sender: Any) {
        self.view.endEditing(true)
        filterSongList(type: "title", otherText: "")
        viewMainPopup.isHidden = true
    }
    @IBAction func btnClear_TitleFilter(_ sender: Any) {
        self.view.endEditing(true)
        clearFilter()
        viewMainPopup.isHidden = true
    }
    
    
    
    //MARK:- UIButton Action LongPress Popup
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began {
            selectLongpreeType = "section"
            selectLongPressDic = arrListSection.object(at: (sender.view?.tag)!) as! NSDictionary
            
            //--
            let favorite = selectLongPressDic.object(forKey: "favorite") as! String
            if favorite == "1" {
                btnFavorites_LongPress.setTitle(NSLocalizedString("Remove From Favorites", comment: ""), for: .normal)
            }else{
                btnFavorites_LongPress.setTitle(NSLocalizedString("Add To Favorites", comment: ""), for: .normal)
            }
            
            let custom_song = selectLongPressDic.object(forKey: "custom_song") as! String
            if custom_song == "1"
            {
                showEditDeleteCustomSongView()
            }
            else
            {
                hiddenEditDeleteCustomSongView()
            }
            
            //--
            lblTitle_LongPress.text = (selectLongPressDic.object(forKey: "title") as! String)
            removeOtherViewToMainPopup()
            viewMain_LongPress.frame = viewMainPopup.frame
            viewMainPopup.addSubview(viewMain_LongPress)
            viewMainPopup.isHidden = false
        }
    }
    @objc func longPressed_row(sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began {
            let p: CGPoint = sender.location(in: tblList)
            var indexPath: IndexPath? = tblList.indexPathForRow(at: p)
            if indexPath == nil {
                // ---
            } else if sender.state == .began {
                let row = indexPath?.row
                let section = indexPath?.section
                print(String(format: "long press on table view at row %ld", section ?? 0))
                selectLongpreeType = "row"
                selectLongPressDic = arrListSection.object(at: (section)!) as! NSDictionary
                
                //--
                let favorite = selectLongPressDic.object(forKey: "favorite") as! String
                if favorite == "1" {
                    btnFavorites_LongPress.setTitle(NSLocalizedString("Remove From Favorites", comment: ""), for: .normal)
                }else{
                    btnFavorites_LongPress.setTitle(NSLocalizedString("Add To Favorites", comment: ""), for: .normal)
                }
                
                let custom_song = selectLongPressDic.object(forKey: "custom_song") as! String
                if custom_song == "1"
                {
                    showEditDeleteCustomSongView()
                }
                else
                {
                    hiddenEditDeleteCustomSongView()
                }
                
                //--
                lblTitle_LongPress.text = (selectLongPressDic.object(forKey: "title") as! String)
                removeOtherViewToMainPopup()
                viewMain_LongPress.frame = viewMainPopup.frame
                viewMainPopup.addSubview(viewMain_LongPress)
                viewMainPopup.isHidden = false
            } else {
                // --
            }
        }
    }
    func shareData(dickey:String)  {
        //--share
        let arr = NSMutableArray()
        arr.add(selectLongPressDic.object(forKey: dickey) as Any)
        let activityViewController = UIActivityViewController(activityItems: arr as! [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = view
        present(activityViewController, animated: true)
    }
    func copyData()  {
        if let title = selectLongPressDic.object(forKey: "title") {
            let id = "\(selectLongPressDic.object(forKey: "id") ?? 0)"
            let lyrics = Database.getLyricsById(id)
            let data : String = (title as! String) + "\n\n" + lyrics
            UIPasteboard.general.string = (data)
            GCGlobal.showTopAlert(message: "Copied", isSuccess: true, completion: nil)
        }
    }
    func youtubeData(dickey:String)  {
        //open youtube
        var YouTubeSearchText:String = ""
        if (UserDefaults.standard.object(forKey: "YouTubeSearchText") != nil)
        {
            YouTubeSearchText = UserDefaults.standard.object(forKey: "YouTubeSearchText") as! String
            YouTubeSearchText = YouTubeSearchText.replacingOccurrences(of: " ", with: "%20")
        }
        var youtubeId = selectLongPressDic.object(forKey: dickey) as! String
        youtubeId = youtubeId.replacingOccurrences(of: " ", with: "%20")
        var urlstring = "youtube://www.youtube.com/results?search_query=" + youtubeId + "%20" + "\(YouTubeSearchText)"
        if !UIApplication.shared.canOpenURL(URL(string:urlstring)!)  {
            urlstring = "http://www.youtube.com/results?search_query=\(youtubeId)" + "%20" + "\(YouTubeSearchText)"
        }
        print("Open " + urlstring)
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL.init(string: urlstring)!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(URL.init(string: urlstring)!)
        }
    }
    @IBAction func btnFavorites_LongPress(_ sender: Any) {
        let favorite = selectLongPressDic.object(forKey: "favorite") as! String
        let id = "\(selectLongPressDic.object(forKey: "id") ?? 0)"
        let dicTemp:NSMutableDictionary = NSMutableDictionary(dictionary: selectLongPressDic)
        if favorite == "1" {
            dicTemp.setValue("0", forKey: "favorite")
            Database.update_fav_song(favorite: "0", strID: id)
        }else{
            dicTemp.setValue("1", forKey: "favorite")
            Database.update_fav_song(favorite: "1", strID: id)
        }
        
        //--
        for i in 0..<arrListSection.count
        {
            let dicTempInner = arrListSection.object(at: i) as! NSDictionary
            let strID = "\(dicTempInner.object(forKey: "id") ?? 0)"
            if strID == id
            {
                arrListSection.removeObject(at: i)
                arrListSection.insert(dicTemp, at: i)
            }
        }
        tblList.reloadData()
        setFontStyleAndSize(label: UILabel(), img: UIImageView(), isFav: "", isReloadtbl: "1")
        viewMainPopup.isHidden = true
    }
    @IBAction func btnShare_LongPress(_ sender: Any) {
        self.view.endEditing(true)
        viewMainPopup.isHidden = true
        if selectLongpreeType == "section" {
            shareData(dickey: "title")
        }else{
            shareData(dickey: "lyrics")
        }
    }
    @IBAction func btnCopytoclipboard_LongPress(_ sender: Any) {
        self.view.endEditing(true)
        viewMainPopup.isHidden = true
        copyData()
    }
    @IBAction func btnYouTube_LongPress(_ sender: Any) {
        self.view.endEditing(true)
        viewMainPopup.isHidden = true
        youtubeData(dickey: "title")
    }
    
    @IBAction func btnEditSongNumber_Logpress(_ sender: Any) {
        self.view.endEditing(true)
        let hymn_number = selectLongPressDic.object(forKey: "hymn_number") as! String
        if hymn_number != DB.MAX_INT as String {
            txt_EditSongNumber.text = hymn_number
        }
        viewMainPopup.isHidden = true
        removeOtherViewToMainPopup()
        viewMain_EditSongNumber.frame = viewMainPopup.frame
        viewMainPopup.addSubview(viewMain_EditSongNumber)
        viewMainPopup.isHidden = false
    }
    
    @IBAction func btnDelete_CustomSong_LognPress(_ sender: Any) {
        let id = "\(selectLongPressDic.object(forKey: "id") ?? 0)"
        Database.deleteTableRow(id)
        arrListSection.remove(selectLongPressDic)
        tblList.reloadData()
        viewMainPopup.isHidden = true
        GCGlobal.showTopAlert(message: NSLocalizedString("Deleted custom song", comment: ""), isSuccess: true, completion: nil)
    }
    @IBAction func btnEdit_CustomSong_LognPress(_ sender: Any) {
        let id = "\(selectLongPressDic.object(forKey: "id") ?? 0)"
        let arrLayrics = Database.retrieveHymnById(id)
        print(arrLayrics)
        var lyrics:String = ""
        if arrLayrics.count != 0 {
            let dictemp = arrLayrics.object(at: 0) as! NSDictionary
            lyrics = (dictemp.object(forKey: "lyrics") as! String)
        }
        txtTitle_InsertCustomSong.text = (selectLongPressDic.object(forKey: "title") as! String)
        txtAuthor_InsertCustomSong.text = (selectLongPressDic.object(forKey: "author") as! String)
        txtNumber_InsertCustomSon.text = (selectLongPressDic.object(forKey: "hymn_number") as! String)
        txtViewLyrics_InsertCustomSong.text = lyrics
        
        isUpdateCustomSong = "1"
        viewMainPopup.isHidden = true
        removeOtherViewToMainPopup()
        viewMain_InsertCustomSong.frame = viewMainPopup.frame
        viewMainPopup.addSubview(viewMain_InsertCustomSong)
        viewMainPopup.isHidden = false
    }
    
    @IBAction func btnBack_Lognpress(_ sender: Any) {
        self.view.endEditing(true)
        viewMainPopup.isHidden = true
    }
    
    //MARK:- UIButtonAction Edit Song Number
    @IBAction func btnSave_EditSongNumber(_ sender: Any) {
        let id = "\(selectLongPressDic.object(forKey: "id") ?? 0)"
        let dicTemp:NSMutableDictionary = NSMutableDictionary(dictionary: selectLongPressDic)
        if txt_EditSongNumber.text?.count != 0
        {
            dicTemp.setValue(txt_EditSongNumber.text, forKey: "hymn_number")
            Database.update_ShowSongNumber(hymn_number: txt_EditSongNumber.text!, strID: id)
            
            //--
            for i in 0..<arrListSection.count
            {
                let dicTempInner = arrListSection.object(at: i) as! NSDictionary
                let strID = "\(dicTempInner.object(forKey: "id") ?? 0)"
                if strID == id
                {
                    arrListSection.removeObject(at: i)
                    arrListSection.insert(dicTemp, at: i)
                }
            }
            
            //-
            if (UserDefaults.standard.object(forKey: "ShowHymnNumber") == nil)
            {
                UserDefaults.standard.set("1", forKey: "ShowHymnNumber")
            }
            selectSongData()
            viewMainPopup.isHidden = true
            txt_EditSongNumber.text = ""
        }
        else
        {
            dicTemp.setValue(DB.MAX_INT, forKey: "hymn_number")
            Database.update_ShowSongNumber(hymn_number: DB.MAX_INT as String, strID: id)
            
            //--
            for i in 0..<arrListSection.count
            {
                let dicTempInner = arrListSection.object(at: i) as! NSDictionary
                let strID = "\(dicTempInner.object(forKey: "id") ?? 0)"
                if strID == id
                {
                    arrListSection.removeObject(at: i)
                    arrListSection.insert(dicTemp, at: i)
                }
            }
            selectSongData()
            viewMainPopup.isHidden = true
            txt_EditSongNumber.text = ""
        }
    }
    @IBAction func btnCancel_EditSongNumber(_ sender: Any) {
        viewMainPopup.isHidden = true
    }
    
    
    //MARK:- UIButton Action
    @IBAction func btnFilterHeader(_ sender: Any) {
        if (viewMainPopup.isHidden) {
            removeOtherViewToMainPopup()
            viewMain_Filter.frame = viewMainPopup.frame
            viewMainPopup.addSubview(viewMain_Filter)
            viewMainPopup.isHidden = false
        }
        else {
            viewMainPopup.isHidden = true
        }
    }
    @IBAction func btnSection_Click(_ sender: UIButton) {
        let dicData = arrListSection.object(at: sender.tag) as! NSDictionary
        let id = "\(dicData.object(forKey: "id") ?? 0)"
        let arrLayrics = Database.retrieveHymnById(id)

        if arrLayrics.count != 0
        {
            if openNewScreenForDetail == 0
            {
                let arrsectionid = arrLayrics.value(forKey: "id") as! NSArray
                let arrrowid = arrListRow.value(forKey: "id") as! NSArray
                if !arrrowid.contains(arrsectionid.object(at: 0))
                {
                    arrListRow.add(arrLayrics.object(at: 0))
                }

                tblList.reloadData()
                let indexPath = IndexPath(row: NSNotFound, section: sender.tag)
                tblList.scrollToRow(at: indexPath, at: .top, animated: false)
            }
            else
            {
                selectSectionIndex = -1
                arrListRow = NSMutableArray()
                tblList.reloadData()
                
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC

                let title = (arrLayrics.object(at: 0) as AnyObject).object(forKey: "title") as! String
                let author = (arrLayrics.object(at: 0) as AnyObject).object(forKey: "author") as! String
                var lyrics = (arrLayrics.object(at: 0) as AnyObject).object(forKey: "lyrics") as! String
                
                lyrics = lyrics.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)

                var detailsContent = title + "\n\n"
                if (!author.isBlank) {
                    detailsContent += author + "\n\n"
                }
                detailsContent += lyrics
                
                newViewController.detailsContent = detailsContent
                newViewController.selectColour = selectColour
                newViewController.tblfontSIZE = tblfontSize
                self.present(newViewController, animated: true, completion: nil)
            }
        }
    }
    @IBAction func btnBackMenu(_ sender: Any) {
        viewMainPopup.isHidden = true
    }
    @IBAction func btnClick_SettingMenu(_ sender: Any) {
        if (UserDefaults.standard.object(forKey: "YouTubeSearchText") != nil)
        {
            txtYoutubeSearchWord_settingMenu.text = (UserDefaults.standard.object(forKey: "YouTubeSearchText") as! String)
        }
        if Common.is_ShowHymnNumber()
        {
            imgCheck_ShowHymnNumber_SettingMenu.image = UIImage.init(named: "ic_check_white")
        }
        else
        {
            imgCheck_ShowHymnNumber_SettingMenu.image = UIImage.init(named: "ic_uncheck_white")
        }
        
        if Common.is_ShowNewWindow() == 1
        {
            imgCheckOpenNewScreen_SettingMenu.image = #imageLiteral(resourceName: "ic_check_white")
        }
        else
        {
            imgCheckOpenNewScreen_SettingMenu.image = #imageLiteral(resourceName: "ic_uncheck_white")
        }
        
        viewMainPopup.isHidden = true
        removeOtherViewToMainPopup()
        viewSettingMenu.frame = viewMainPopup.frame
        viewMainPopup.addSubview(viewSettingMenu)
        viewMainBg_Color.bringSubviewToFront(viewMainPopup)
        viewMainPopup.isHidden = false
    }
    
    @IBAction func btnAbout_Menu(_ sender: Any) {
        let totalCount = Database.countTblData(customOnly: false)
        lblTotalSong.text = "\(totalCount)" + " " + "total songs in database"
        lblDisplaySong.text = "\(arrListSection.count)" + " " + "songs displayed"
        viewMainPopup.isHidden = true
        removeOtherViewToMainPopup()
        viewMain_About.frame = viewMainPopup.frame
        viewMainPopup.addSubview(viewMain_About)
        viewMainPopup.isHidden = false
    }

    @IBAction func btnSubmitHymn_Menu(_ sender: Any) {
        let url = getConfigValueFromPlist("submitHymnUrl")
        openURL(url)
        viewMainPopup.isHidden = true
    }

    @IBAction func btnHymnLyricsApp_Menu(_ sender: Any) {
        let url = getConfigValueFromPlist("hymnLyricsAppUrl")
        openURL(url)
        viewMainPopup.isHidden = true
    }

    @IBAction func btnRateApp_Menu(_ sender: Any) {
        let url = getConfigValueFromPlist("rateAppUrl")
        openURL(url)
        viewMainPopup.isHidden = true
    }
    
    func getConfigValueFromPlist(_ key: String) -> String {
        var dicPlist: NSDictionary?
        var value: String = ""
        
        if Bundle.main.bundleIdentifier == Common.hymnlyricsfree_identifire{
            if let path = Bundle.main.path(forResource: "HymnLyricsFreeInfo", ofType: "plist") {
                dicPlist = NSDictionary(contentsOfFile: path)
                value = dicPlist?.object(forKey: key) as! String
            }
        } else if Bundle.main.bundleIdentifier == Common.hymnlyricsplus_identifire{
            if let path = Bundle.main.path(forResource: "HymnLyricsPlusInfo", ofType: "plist") {
                dicPlist = NSDictionary(contentsOfFile: path)
                value = dicPlist?.object(forKey: key) as! String
            }
        } else {
            if let path = Bundle.main.path(forResource: "HymnLyricsPlusAdsInfo", ofType: "plist") {
                dicPlist = NSDictionary(contentsOfFile: path)
                value = dicPlist?.object(forKey: key) as! String
            }
        }
        return value
    }
    
    func openURL(_ url: String) {
        if (url.length > 0) {
            let appURL = URL(string: url)!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appURL as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }
            else {
                UIApplication.shared.openURL(appURL as URL)
            }
        }
    }
    
    @IBAction func btnCustomSong_Menu(_ sender: Any) {
        viewMainPopup.isHidden = true
        removeOtherViewToMainPopup()
        viewMain_CustomSong.frame = viewMainPopup.frame
        viewMainPopup.addSubview(viewMain_CustomSong)
        viewMainPopup.isHidden = false
    }
    
    @IBAction func btnFeedbackLink(_ sender: Any) {
        sendEmail()
        viewMainPopup.isHidden = true
    }
    @IBAction func btnOK_About(_ sender: Any) {
        self.view.endEditing(true)
        viewMainPopup.isHidden = true
    }
    @IBAction func btnBack_Aboutmenu(_ sender: Any) {
        self.view.endEditing(true)
        viewMainPopup.isHidden = true
    }
    
    @IBAction func btnStyle_SettingMenu(_ sender: Any) {
        let lblNotificationText = sender as? UIButton
        let chooseArticleDropDown = DropDown()
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText?.bounds.height)!)
        chooseArticleDropDown.dataSource = Settings.SCREEN_STYLES
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            //lblNotificationText?.setTitle(item, for: .normal)
            self?.lblStyle_SettingMenu.text = item
            chooseArticleDropDown.hide()
        }
        chooseArticleDropDown.multiSelectionAction = { (indices, items) in
            if items.isEmpty {
                lblNotificationText?.setTitle("", for: .normal)
            }
        }
        chooseArticleDropDown.show()
    }
    @IBAction func btnFontSize_SettingMenu(_ sender: UIButton) {
        let lblNotificationText = sender
        let chooseArticleDropDown = DropDown()
        chooseArticleDropDown.anchorView = lblNotificationText
        chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: (lblNotificationText.bounds.height))
        
        chooseArticleDropDown.dataSource = Settings.FONT_SIZES
        chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
            //lblNotificationText?.setTitle(item, for: .normal)
            self?.lblFontSize_SettingMenu.text = item
            
            chooseArticleDropDown.hide()
        }
        chooseArticleDropDown.multiSelectionAction = { (indices, items) in
            print("Muti selection action called with: \(items)")
            if items.isEmpty {
                lblNotificationText.setTitle("", for: .normal)
            }
        }
        chooseArticleDropDown.show()
    }
    @IBAction func btnNewScreen_SettingMenu(_ sender: Any) {
        if openNewScreenForDetail == 0
        {
            openNewScreenForDetail = 1
            imgCheckOpenNewScreen_SettingMenu.image = #imageLiteral(resourceName: "ic_check_white")
        }
        else
        {
            openNewScreenForDetail = 0
            imgCheckOpenNewScreen_SettingMenu.image = #imageLiteral(resourceName: "ic_uncheck_white")
        }
        UserDefaults.standard.set(openNewScreenForDetail, forKey: "openNewScreen")
    }
    @IBAction func btnShowHymnNumber_SettingMenu(_ sender: Any) {
        if Common.is_ShowHymnNumber()
        {
            imgCheck_ShowHymnNumber_SettingMenu.image = UIImage.init(named: "ic_uncheck_white")
            UserDefaults.standard.set("0", forKey: "ShowHymnNumber")
        }
        else
        {
            imgCheck_ShowHymnNumber_SettingMenu.image = UIImage.init(named: "ic_check_white")
            UserDefaults.standard.set("1", forKey: "ShowHymnNumber")
        }
    }
    @IBAction func btnOk_SettingMenu(_ sender: Any) {
        UserDefaults.standard.set(lblFontSize_SettingMenu.text, forKey: "fontSize")
        UserDefaults.standard.set(lblStyle_SettingMenu.text, forKey: "fontStyle")
        UserDefaults.standard.set(txtYoutubeSearchWord_settingMenu.text, forKey: "YouTubeSearchText")
        UserDefaults.standard.set(openNewScreenForDetail, forKey: "openNewScreen")
        UserDefaults.standard.synchronize()
        
        setFontStyleAndSize(label: UILabel(), img: UIImageView(), isFav: "", isReloadtbl: "1")
        viewMainPopup.isHidden = true
    }
    @IBAction func btnBack_SettingMenu(_ sender: Any) {
        viewMainPopup.isHidden = true
    }
    @IBAction func btnMenu(_ sender: Any) {
        if (viewMainPopup.isHidden) {
            removeOtherViewToMainPopup()
            viewMenu.frame = viewMainPopup.frame
            viewMainPopup.addSubview(viewMenu)
            viewMainPopup.isHidden = false
        }
        else {
            viewMainPopup.isHidden = true
        }
    }
    //MARK:- Action Custom song
    @IBAction func btnSave_InsertCustomSong(_ sender: Any) {
        
        if (txtTitle_InsertCustomSong.text?.isEmpty)! {
            GCGlobal.showTopAlert(message: "Enter Title.", isSuccess: false, completion: nil)
            return
        }
        if (txtViewLyrics_InsertCustomSong.text?.isEmpty)! {
            GCGlobal.showTopAlert(message: "Enter Lyrics.", isSuccess: false, completion: nil)
            return
        }
        
        //---Insert
        let title = txtTitle_InsertCustomSong.text
        let lyrics = txtViewLyrics_InsertCustomSong.text
        let author = txtAuthor_InsertCustomSong.text
        let hymn_number = txtNumber_InsertCustomSon.text
        let keywords = ""
        let background = ""
        let tune = ""
        let lastUpdate = ""
        
        
        if isUpdateCustomSong == "1" {
            let strID = "\(selectLongPressDic.object(forKey: "id") ?? 0)"
            
            Database.updateTableRow(id: strID, title: title!, lyrics: lyrics!, author: author!, hymnNumber: hymn_number!)
            
            GCGlobal.showTopAlert(message: "Updated custom song", isSuccess: true, completion: nil)
            
        }else{
            Database.insertDataTbl(title: title!, lyrics: lyrics!, tune: tune, lastUpdate: lastUpdate, author: author!, keyword: keywords, background: background, hymn_number: hymn_number!, customsong: "1")
            
            GCGlobal.showTopAlert(message: "Added custom song", isSuccess: true, completion: nil)
        }
        
        selectSongData()
        
        viewMainPopup.isHidden = true
        txtTitle_InsertCustomSong.text = ""
        txtAuthor_InsertCustomSong.text = ""
        txtViewLyrics_InsertCustomSong.text = ""
        txtNumber_InsertCustomSon.text = ""
    }
    @IBAction func btnCancel_InsertCutomSong(_ sender: Any) {
        viewMainPopup.isHidden = true
    }
    
    @IBAction func btnAdd_CustomSong(_ sender: Any) {
        viewMainPopup.isHidden = true
        isUpdateCustomSong = "0"
        removeOtherViewToMainPopup()
        viewMain_InsertCustomSong.frame = viewMainPopup.frame
        viewMainPopup.addSubview(viewMain_InsertCustomSong)
        viewMainPopup.isHidden = false
    }
    
    @IBAction func btnLoad_CustomSong(_ sender: Any) {
        viewMainPopup.isHidden = true
        let types = [kUTTypePDF, kUTTypeText, kUTTypeRTF, kUTTypeSpreadsheet]
        //["public.composite-content"]
        let documentPicker = UIDocumentPickerViewController(documentTypes: types as [String], in: .import)
        documentPicker.delegate = self
        documentPicker.modalPresentationStyle = .formSheet
        present(documentPicker, animated: true)
    }
    
    @IBAction func btnBackup_CustomSong(_ sender: Any) {
        let arr = Database.retrieveHymnData(filterByCol: DB.CUSTOM_SONG, filterByText: "1", returnLyrics: true)
        if arr.count == 0 {
            viewMainPopup.isHidden = true
        } else {
            var strTemp = ""
            for obj in arr {
                if let data = obj as? [String: Any] {
                    strTemp += "Title: " + "\(String(describing: data["title"]!))" + "\n"
                    
                    if (data["author"] != nil) {
                        strTemp += "Author: " + "\(String(describing: data["author"]!))" + "\n"
                    }
                    
                    if (data["keyword"] != nil) {
                        strTemp += "Keywords: " + "\(String(describing: data["keyword"]!))" + "\n"
                    }
                    
                    if (data["tune"] != nil) {
                        strTemp += "Tune: " + "\(String(describing: data["tune"]!))" + "\n"
                    }
                    
                    if (data["last_update"] != nil) {
                        strTemp += "LastUpdate: " + "\(String(describing: data["last_update"]!))" + "\n"
                    }
                    
                    if (data["background"] != nil) {
                        strTemp += "Background: " + "\(String(describing: data["background"]!))" + "\n"
                    }
                   
                    if (data["lyrics"] != nil) {
                        strTemp += "Lyrics: \n" + "\(String(describing: data["lyrics"]!))" + "\n"
                    }
                    strTemp += "---" + "\n"
                }
            }
            write(text: strTemp, to: "HymnLyricsCustomSongs")
        }
    }
    
    func write(text: String, to fileNamed: String, folder: String = "SavedFiles") {
        guard let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else {
            viewMainPopup.isHidden = true
            return
        }
        guard let writePath = NSURL(fileURLWithPath: path).appendingPathComponent(folder) else {
            viewMainPopup.isHidden = true
            return
        }
        do {
            try FileManager.default.createDirectory(atPath: writePath.path, withIntermediateDirectories: true)
            let file = writePath.appendingPathComponent(fileNamed + ".txt")
            try text.write(to: file, atomically: false, encoding: String.Encoding.utf8)
            
            let fileURL = URL(fileURLWithPath: file.path)
            
            viewMainPopup.isHidden = true
            GCSocialShare.shared.socialShare(sender: self, sView: self.view, sharingText: nil, sharingImage: nil, sharingURL: fileURL, sharingURL1: nil, completion: nil)
        } catch _ {
            viewMainPopup.isHidden = true
        }
    }
    
}
extension ViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        var newString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        if textField == txtJumpTo
        {
            selectJumpIndex = 0
            
            if (newString?.count)! > 0 && arrListSection.count > 0
            {
                for i in 1..<arrListSection.count
                {
                    let dicTemp = arrListSection.object(at: i) as! NSDictionary
                    var title = dicTemp.object(forKey: "title") as! String
                    title = title.capitalized
                    newString = newString?.capitalized
                    if title.starts(with: newString!)
                    {
                        if selectJumpIndex == 0
                        {
                            selectJumpIndex = i
                        }
                    }
                }
                if selectJumpIndex > 0
                {
                    let indexPath = IndexPath(row: NSNotFound, section: selectJumpIndex)
                    tblList.scrollToRow(at: indexPath, at: .top, animated: false)
                }
            }
        }else if textField == txt_EditSongNumber{
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}


extension ViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tblList {
            return arrListSection.count
        }else{
            //filter by category
            return 1
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if tableView == tblList {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_section") as! SongSectionVC
            cell.selectionStyle = .none
            let dicData = arrListSection.object(at: section) as! NSDictionary
            
            let hymn_number = (dicData.object(forKey: "hymn_number") as! String)
            let title = (dicData.object(forKey: "title") as! String)
            
            var titleTemp = String()
            if hymn_number != DB.MAX_INT as String
            {
                if Common.is_ShowHymnNumber()
                {
                    titleTemp = hymn_number + " - " + title
                }
                else
                {
                    titleTemp = title
                }
            }
            else
            {
                titleTemp = title
            }
            cell.lblTitle.text = titleTemp
            
            //--
            let fv = (dicData.object(forKey: "favorite") as! String)
            if fv == "1" {
                cell.imgFav.image = UIImage.init(named: "ic_fv.png")
            }else{
                cell.imgFav.image = nil
            }
            
            lbltitle_ScrollTimeDisplay.text = String(title.prefix(1))
            
            //--
            setFontStyleAndSize(label: cell.lblTitle, img: cell.imgFav, isFav: fv, isReloadtbl: "")
            cell.btnSectionClick.tag = section
            
            //--
            cell.lblbottomLine.isHidden = false
            let sectionid = (arrListSection.object(at: section) as! NSDictionary).object(forKey: "id") as! Int
            for i in 0..<arrListRow.count
            {
                let arrrdic = arrListRow.object(at: i) as! NSDictionary
                if Int(arrrdic.object(forKey: "id") as! Int) == Int(sectionid)
                {
                    cell.lblbottomLine.isHidden = true
                }
            }
            
            //--
            longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(sender:)))
            longPressRecognizer.minimumPressDuration = 1.0
            longPressRecognizer.delegate = self
            longPressRecognizer.view?.tag = section
            cell.btnSectionClick.addGestureRecognizer(longPressRecognizer)
            return cell
        }else{
            //filter by category
            return nil
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblList {
            let sectionid = (arrListSection.object(at: section) as! NSDictionary).object(forKey: "id") as! Int
            let arrrowid = arrListRow.value(forKey: "id") as! NSArray
            if arrrowid.contains(sectionid)
            {
                return 1
            }
        }else{
            return arrlist_categoryFilter.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblList {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_row", for: indexPath as IndexPath)
            cell.selectionStyle = .none
            
            var dicData = NSDictionary()
            
            let sectionid = (arrListSection.object(at: indexPath.section) as! NSDictionary).object(forKey: "id") as! Int
            for i in 0..<arrListRow.count
            {
                let arrrdic = arrListRow.object(at: i) as! NSDictionary
                if Int(arrrdic.object(forKey: "id") as! Int) == Int(sectionid)
                {
                    dicData = arrListRow.object(at: i) as! NSDictionary
                }
            }
            
            let author = (dicData.object(forKey: "author") as! String)
            var detail = (dicData.object(forKey: "lyrics") as! String)
            detail = detail.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
            
            //--
            let lblDetail = cell.contentView.viewWithTag(11) as! UILabel
            if author.count != 0
            {
                lblDetail.text = author + "\n\n" + detail
            }
            else
            {
                lblDetail.text = detail
            }
            
            //--
            setFontStyleAndSize(label: lblDetail, img: UIImageView(), isFav: "", isReloadtbl: "")
            
            return cell
        }else{
            //filter by category
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell_row", for: indexPath as IndexPath)
            cell.selectionStyle = .none
            let lbl = cell.contentView.viewWithTag(101) as! UILabel
            lbl.text = (arrlist_categoryFilter[indexPath.row] as! String)
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblList {
            let sectionid = (arrListSection.object(at: indexPath.section) as! NSDictionary).object(forKey: "id") as! Int
            for i in 0..<arrListRow.count
            {
                let arrrdic = arrListRow.object(at: i) as! NSDictionary
                if Int(arrrdic.object(forKey: "id") as! Int) == Int(sectionid)
                {
                    arrListRow.removeObject(at: i)
                    break
                }
            }
            tblList.reloadData()
            
            //--
            let indexPath1 = IndexPath(row: NSNotFound, section: indexPath.section)
            tblList.scrollToRow(at: indexPath1, at: .top, animated: false)
            
        }else{
            //filter by category
            
            filterSongList(type: "category", otherText: arrlist_categoryFilter[indexPath.row] as! String)
            viewMainPopup.isHidden = true
        }
    }
    
}

extension ViewController: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if scrollView == tblList
        {
            viewMainPopup.isHidden = true
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tblList
        {
           
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
