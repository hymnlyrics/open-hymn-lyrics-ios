//
//  database.swift
//
//  Copyright © 2018 Ecodia. All rights reserved.
//

import UIKit
import SQLite3
class Database
{
    static var db: OpaquePointer?
    
    class func createDB() {
        if (db == nil) {
            openDB()
        }
        
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent(DB.HYMN_DATABASE)
        
        // open database
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }
        //create table
        if sqlite3_exec(db, "create table if not exists " + DB.HYMN_LYRICS_TABLE + " (" + DB.ID + " integer primary key autoincrement, " + DB.TITLE + " text, " + DB.LYRICS + " text, " + DB.TUNE + " text, " + DB.LAST_UPDATE + " text, " + DB.AUTHOR + " text, " + DB.KEYWORD + " text, " + DB.BACKGROUND + " text, " + DB.FAVORITE + " text, " + DB.HYMN_NUMBER + " text," + DB.CUSTOM_SONG + " text," + DB.SORT_TITLE + " text," + DB.YEAR + " text," + DB.TEXT_2 + " text," + DB.TEXT_3 + " text," + DB.TEXT_4 + " text," + DB.TEXT_5 + " text" + ")", nil, nil, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error creating table: \(errmsg)")
        }
    }
    class func openDB() {
        if (db == nil) {
            let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                .appendingPathComponent(DB.HYMN_DATABASE)
            if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
                print("error opening database")
            }
        }
    }
    
    class func getIdOfSongTitle(title:String) -> Int32 {
        let queryStatementString = "SELECT " + DB.ID + " FROM " + DB.HYMN_LYRICS_TABLE + " where " + DB.TITLE + " = ?"
        var queryStatement: OpaquePointer? = nil
        var id : Int32 = -1
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(queryStatement, 1, title, -1, nil)

            while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                id = sqlite3_column_int(queryStatement, 0)
            }
            sqlite3_finalize(queryStatement)
        }
        else {
            print("SELECT statement could not be prepared: \(queryStatementString)")
        }
        return id
    }
    
    class func insertDataTbl(title:String,lyrics:String,tune:String,lastUpdate:String,author:String,keyword:String,background:String,hymn_number:String,customsong:String)
    {
        let insertStatementString = "INSERT INTO " + DB.HYMN_LYRICS_TABLE + " (" + DB.TITLE + "," + DB.FAVORITE + "," + DB.AUTHOR + "," + DB.LYRICS + "," + DB.KEYWORD + "," + DB.TUNE + "," + DB.LAST_UPDATE + "," + DB.BACKGROUND + "," + DB.HYMN_NUMBER + "," + DB.CUSTOM_SONG + ") VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"
        var insertStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            let title: NSString = title as NSString
            let lyrics: NSString = lyrics as NSString
            let tune: NSString = tune as NSString
            let lastUpdate: NSString = lastUpdate as NSString
            let author: NSString = author as NSString
            let keyword: NSString = keyword as NSString
            let background: NSString = background as NSString
            let favorite: NSString = "0"
            let hymn_number: NSString? = hymn_number as NSString
            let customsong:NSString = customsong as NSString
            
            var index : Int32 = 1
            sqlite3_bind_text(insertStatement, index, title.utf8String, -1, nil)
            index += 1
            sqlite3_bind_text(insertStatement, index, favorite.utf8String, -1, nil)
            index += 1
            sqlite3_bind_text(insertStatement, index, author.utf8String, -1, nil)
            index += 1
            sqlite3_bind_text(insertStatement, index, lyrics.utf8String, -1, nil)
            index += 1
            sqlite3_bind_text(insertStatement, index, keyword.utf8String, -1, nil)
            index += 1
            sqlite3_bind_text(insertStatement, index, tune.utf8String, -1, nil)
            index += 1
            sqlite3_bind_text(insertStatement, index, lastUpdate.utf8String, -1, nil)
            index += 1
            sqlite3_bind_text(insertStatement, index, background.utf8String, -1, nil)
            index += 1
            sqlite3_bind_text(insertStatement, index, hymn_number!.utf8String, -1, nil)
            index += 1
            sqlite3_bind_text(insertStatement, index, customsong.utf8String, -1, nil)

            let status = sqlite3_step(insertStatement)
            if status != SQLITE_DONE {
                let errmsg = String(cString: sqlite3_errmsg(db)!)
                print("\(status) - \(errmsg)")
            }
        } else {
           print("INSERT statement could not be prepared.")
        }
        sqlite3_finalize(insertStatement)
    }
    
    class func countTblData(customOnly: Bool = false, builtinOnly: Bool = false) -> Int32 {
        var queryStatementString = "SELECT count(*) FROM " + DB.HYMN_LYRICS_TABLE
        if (customOnly) {
            queryStatementString += " where " + DB.CUSTOM_SONG + " = 1"
        }
        else if (builtinOnly) {
            queryStatementString += " where " + DB.CUSTOM_SONG + " = 0"
        }
        var queryStatement: OpaquePointer? = nil
        var count : Int32 = 0
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                count = sqlite3_column_int(queryStatement, 0)
            }
            sqlite3_finalize(queryStatement)
        }
        else {
            print("SELECT statement could not be prepared")
        }
        return count
    }
    
    class func retrieveHymnById(_ id: String) -> NSMutableArray {
        let sql = "SELECT " + DB.ID + "," + DB.TITLE + "," + DB.LYRICS + "," + DB.FAVORITE + "," + DB.AUTHOR + "," +
            DB.HYMN_NUMBER + "," + DB.CUSTOM_SONG + " FROM " + DB.HYMN_LYRICS_TABLE +
            " where " + DB.ID + "=" + id

        let results : NSMutableArray = Database.selectTblData(queryStatementString: sql)
        
        return results
    }
        
    class func retrieveHymnData(filterByCol: String = "", filterByText: String = "", showHymnNumber: Bool = false, returnLyrics: Bool = false) -> NSMutableArray {
        var results : NSMutableArray
        var whereString : String = "1 = 1"
        var orderString : String = DB.TITLE
        
        if (filterByCol == DB.TITLE) {
            whereString = DB.TITLE + " LIKE '%" + filterByText + "%'"
        }
        else if (filterByCol == DB.LYRICS) {
            whereString = DB.LYRICS + " LIKE '%" + filterByText + "%'"
        }
        else if (filterByCol == DB.KEYWORD) {
            whereString = DB.KEYWORD + " LIKE '%" + filterByText + "%'"
        }
        else if (filterByCol == DB.CUSTOM_SONG) {
            if (filterByText == "1") {
                whereString = DB.CUSTOM_SONG + " = 1"
            }
            else {
                whereString = DB.CUSTOM_SONG + " = 0"
            }
        }
        else if (filterByCol == DB.FAVORITE) {
            whereString = DB.FAVORITE + " = 1"
        }

        if (showHymnNumber) {
            orderString = "CAST(" + DB.HYMN_NUMBER + " AS INTEGER), " + DB.TITLE
        }

        var cols = DB.ID + "," + DB.TITLE + "," + DB.FAVORITE + "," + DB.AUTHOR + "," +
            DB.HYMN_NUMBER + "," + DB.CUSTOM_SONG
        
        if (returnLyrics) {
            cols = cols + "," + DB.LYRICS
        }
        
        let sql = "SELECT " + cols +
            " FROM " + DB.HYMN_LYRICS_TABLE +
            " where " + whereString +
            " order by " + orderString
        
        results = Database.selectTblData(queryStatementString: sql)
        return results
    }
    
    class func selectTblData(queryStatementString: String) -> NSMutableArray {
        var queryStatement: OpaquePointer? = nil
        let arrList:NSMutableArray = NSMutableArray()
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            
            while (sqlite3_step(queryStatement) == SQLITE_ROW) {
                let id = sqlite3_column_int(queryStatement, 0)
                
                let dictemp:NSMutableDictionary = NSMutableDictionary()
                dictemp.setValue(id, forKey: DB.ID)
                
                for i in 1..<10
                {
                    let rowtext = sqlite3_column_text(queryStatement, Int32(i))
                    let rowColumnName = sqlite3_column_name(queryStatement, Int32(i))
                    if (rowColumnName != nil)
                    {
                        if String(cString: rowColumnName!) == DB.TITLE
                        {
                            if (rowtext != nil)
                            {
                                let title = String(cString: rowtext!)
                                dictemp.setValue(title, forKey: DB.TITLE)
                            }
                        }
                        else if String(cString: rowColumnName!) == DB.FAVORITE
                        {
                            if (rowtext != nil)
                            {
                                let favorite = String(cString: rowtext!)
                                dictemp.setValue(favorite, forKey: DB.FAVORITE)
                            }
                        }
                        else if String(cString: rowColumnName!) == DB.AUTHOR
                        {
                            if (rowtext != nil)
                            {
                                let author = String(cString: rowtext!)
                                dictemp.setValue(author, forKey: DB.AUTHOR)
                            }
                        }
                        else if String(cString: rowColumnName!) == DB.LYRICS
                        {
                            if (rowtext != nil)
                            {
                                let lyrics = String(cString: rowtext!)
                                dictemp.setValue(lyrics, forKey: DB.LYRICS)
                            }
                        }
                        else if String(cString: rowColumnName!) == DB.KEYWORD
                        {
                            if (rowtext != nil)
                            {
                                let keyword = String(cString: rowtext!)
                                dictemp.setValue(keyword, forKey: DB.KEYWORD)
                            }
                        }
                        else if String(cString: rowColumnName!) == DB.TUNE
                        {
                            if (rowtext != nil)
                            {
                                let tune = String(cString: rowtext!)
                                dictemp.setValue(tune, forKey: DB.TUNE)
                            }
                        }
                        else if String(cString: rowColumnName!) == DB.LAST_UPDATE
                        {
                            if (rowtext != nil)
                            {
                                let lastUpdate = String(cString: rowtext!)
                                dictemp.setValue(lastUpdate, forKey: DB.LAST_UPDATE)
                            }
                        }
                        else if String(cString: rowColumnName!) == DB.BACKGROUND
                        {
                            if (rowtext != nil)
                            {
                                let background = String(cString: rowtext!)
                                dictemp.setValue(background, forKey: DB.BACKGROUND)
                            }
                        }
                        else if String(cString: rowColumnName!) == DB.HYMN_NUMBER
                        {
                            if (rowtext != nil)
                            {
                                let hymn_number = String(cString: rowtext!)
                                dictemp.setValue(hymn_number, forKey: DB.HYMN_NUMBER)
                            }
                        }
                        else if String(cString: rowColumnName!) == DB.CUSTOM_SONG
                        {
                            if (rowtext != nil)
                            {
                                let hymn_number = String(cString: rowtext!)
                                dictemp.setValue(hymn_number, forKey: DB.CUSTOM_SONG)
                            }
                        }
                    }
                }

                arrList.add(dictemp)
            }
            
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return arrList
    }
    
    class func update_fav_song(favorite: String, strID: String) {
        let updateStatementString = "UPDATE " + DB.HYMN_LYRICS_TABLE + " SET favorite = '" + favorite + "' WHERE " + DB.ID + " = " + strID + ";"
        
        var updateStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            if sqlite3_step(updateStatement) == SQLITE_DONE {
                print("Successfully updated row.")
            } else {
                print("Could not update row.")
            }
        } else {
            print("UPDATE statement could not be prepared")
        }
        sqlite3_finalize(updateStatement)
    }
    
    class func update_ShowSongNumber(hymn_number: String, strID: String) {
        let updateStatementString = "UPDATE " + DB.HYMN_LYRICS_TABLE + " SET hymn_number = '" + hymn_number + "' WHERE " + DB.ID + " = " + strID + ";"
        
        var updateStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            if sqlite3_step(updateStatement) == SQLITE_DONE {
                print("Successfully updated row.")
            } else {
                print("Could not update row.")
            }
        } else {
            print("UPDATE statement could not be prepared")
        }
        sqlite3_finalize(updateStatement)
    }
    
    class func updateTableRow(id: String, title: String, lyrics: String, author: String, hymnNumber: String) {
        
        let updateStatementString = "UPDATE " + DB.HYMN_LYRICS_TABLE + " SET " + DB.TITLE + "='" + title + "'," +
        DB.LYRICS + "='" + lyrics + "'," + DB.AUTHOR + "='" + author + "'," + DB.HYMN_NUMBER + "='" + hymnNumber +
            "' WHERE " + DB.ID + "=" + id
        
        var updateStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            if sqlite3_step(updateStatement) == SQLITE_DONE {
                print("Successfully updated row.")
            } else {
                print("Could not update row.")
            }
        } else {
            print("UPDATE statement could not be prepared")
        }
        sqlite3_finalize(updateStatement)
    }
    
    class func resetDB()
    {
        let updateStatementString = "DELETE FROM " + DB.HYMN_LYRICS_TABLE + ";"
        
        var updateStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            if sqlite3_step(updateStatement) == SQLITE_DONE {
                print("Successfully deleted all data")
            } else {
                print("Could not delete data")
            }
        } else {
            print("Delete statement could not be prepared")
        }
        sqlite3_finalize(updateStatement)
        createDB()
        
    }
    
    class func deleteTableRow(_ id: String)
    {
        let updateStatementString = "DELETE FROM " + DB.HYMN_LYRICS_TABLE + "  WHERE " + DB.ID + "=" + id
        var updateStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            if sqlite3_step(updateStatement) == SQLITE_DONE {
                print("Successfully deleted all data")
            } else {
                print("Could not delete data")
            }
        } else {
            print("Delete statement could not be prepared")
        }
        sqlite3_finalize(updateStatement)
        createDB()
    }
    
    class func getLyricsById(_ id:String) -> String {
        let queryS = "SELECT id,lyrics FROM " + DB.HYMN_LYRICS_TABLE + " where id = " + id
        let arrLayrics = selectTblData(queryStatementString: queryS)
        var lyrics:String = ""
        if arrLayrics.count != 0 {
            let dictemp = arrLayrics.object(at: 0) as! NSDictionary
            lyrics = (dictemp.object(forKey: "lyrics") as! String)
        }
        return lyrics
    }
}
