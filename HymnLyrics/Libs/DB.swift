//
//  DB.swift
//  HymnLyrics
//
//  Created by Tseng, Oliver on 12/18/18.
//  Copyright © 2018 Ecodia. All rights reserved.
//

import Foundation

struct DB {
    
    static let HYMN_DATABASE = "hymn_data.sqlite"
    
    static let HYMN_LYRICS_TABLE = "hymn_lyrics"
    static let ID = "id"
    static let TITLE = "title"
    static let LYRICS = "lyrics"
    static let FAVORITE = "favorite"
    static let AUTHOR = "author"
    static let KEYWORD = "keyword"
    static let TUNE = "tune"
    static let BACKGROUND = "background"
    static let LAST_UPDATE = "last_update"
    static let HYMN_NUMBER = "hymn_number"
    static let CUSTOM_SONG = "custom_song"
    static let SORT_TITLE = "sort_title"
    static let YEAR = "year"
    static let TEXT_2 = "text_2"
    static let TEXT_3 = "text_3"
    static let TEXT_4 = "text_4"
    static let TEXT_5 = "text_5"

    static let MAX_INT = "0987654321234567890987654"
    
    
}
