//
//  DBStatus.swift
//  HymnLyrics
//
//  Created by Tseng, Oliver on 12/24/18.
//  Copyright © 2018 Ecodia. All rights reserved.
//

import Foundation

struct DBStatus {
    var inserted : Int32 = 0
    var skipped : Int32 = 0
}


