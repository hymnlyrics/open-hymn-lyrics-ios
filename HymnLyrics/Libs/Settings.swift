//
//  Settings.swift
//  HymnLyrics
//
//  Created by Tseng, Oliver on 12/25/18.
//  Copyright © 2018 Ecodia. All rights reserved.
//

import Foundation

struct Settings {
    
    static let SCREEN_STYLES = ["Black/White","Black/Gray","Blue/White","Blue/Black","Brown/Black", "Gray/Black", "Green/Black","Magenta/Black","Pink/White","Pink/Black", "White/Black", "White/Gray", ]
    
    static let FONT_SIZES = ["XS","S","M","L","XL"]
}
