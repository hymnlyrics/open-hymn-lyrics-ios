//
//  SongSectionVC.swift
//  SongLyrics
//
//  Created by Maulik Vinodbhai Vora on 30/11/18.
//  Copyright © 2018 Ecodia. All rights reserved.
//

import UIKit

class SongSectionVC: UITableViewCell {

    
    @IBOutlet weak var lblbottomLine: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var imgFav: UIImageView!
    @IBOutlet weak var btnSectionClick: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
