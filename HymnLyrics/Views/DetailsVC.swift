/**
 * Copyright (c) 2018 Ecodia
 */
import UIKit

class DetailsVC: UIViewController {

    @IBOutlet weak var detailsTxtVIew: UITextView!
    var detailsContent : String = ""
    
    
    //--
    var tblfontSIZE:Int = Int()
    var selectColour = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        detailsTxtVIew.text = detailsContent
        detailsTxtVIew.font = detailsTxtVIew.font?.withSize(CGFloat(tblfontSIZE))
        
        let fgColor = Common.getForegroundColor(selectColour)
        let bgColor = Common.getBackgroundColor(selectColour)
        
        detailsTxtVIew.textColor = fgColor
        detailsTxtVIew.backgroundColor = bgColor
        
        detailsTxtVIew.scrollToTop()
    }

    @IBAction func BtnClickBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
