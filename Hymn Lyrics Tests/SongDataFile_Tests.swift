//
//  SongDataFile_Tests.swift
//  Hymn Lyrics Tests
//
//  Created by Tseng, Oliver on 12/26/18.
//  Copyright © 2018 Ecodia. All rights reserved.
//

import Foundation

import XCTest
@testable import HymnLyrics

class SongDataFile_Tests: XCTestCase {
    
    var viewController: ViewController!
    
    override func setUp() {
        viewController = ViewController()
    }
    
    override func tearDown() {
    }
    
    func test_checkSongCount() {
        viewController.loadFromDatabase()
        XCTAssertTrue(viewController.arrListSection.count > 500)
    }
}

