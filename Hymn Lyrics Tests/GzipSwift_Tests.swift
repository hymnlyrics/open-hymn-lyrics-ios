//
//  GzipSwift_Tests.swift
//  Hymn Lyrics Tests
//
//  Created by Tseng, Oliver on 12/28/18.
//  Copyright © 2018 Ecodia. All rights reserved.
//

import Foundation
import XCTest
import Gzip

@testable import HymnLyrics

class GzipSwift_Tests: XCTestCase {
    
    override func setUp() {
    }
    
    override func tearDown() {
    }
    
    func testGZip() {
        
        let testSentence = "foo"
        
        let data = testSentence.data(using: .utf8)!
        let gzipped = try! data.gzipped()
        let uncompressed = try! gzipped.gunzipped()
        let uncompressedSentence = String(data: uncompressed, encoding: .utf8)
        
        XCTAssertNotEqual(gzipped, data)
        XCTAssertEqual(uncompressedSentence, testSentence)
        
        XCTAssertTrue(gzipped.isGzipped)
        XCTAssertFalse(data.isGzipped)
        XCTAssertFalse(uncompressed.isGzipped)
    }
    
    
    func testUnencode() {
        
        let original = "Original Test String"

        let originalData = Data(original.utf8)
        let encoded = originalData.base64EncodedData()
        
        let encodedData = Data(base64Encoded: encoded, options: Data.Base64DecodingOptions(rawValue: 0))
        let decoded = String(data: encodedData as! Data, encoding: String.Encoding.utf8)
        
        XCTAssertEqual(decoded, original)

    }
    
    func testZipEncode() {
        
        let testSentence = "This test should produce a string"
        
        let data = testSentence.data(using: .utf8)!
        let gzipped = try! data.gzipped()
        
        let encoded = gzipped.base64EncodedData()
        
        let output = String(data: encoded, encoding: .utf8)
        
        print(output!)

//        let encodedData = Data(base64Encoded: encoded, options: Data.Base64DecodingOptions(rawValue: 0))
//
//
//        let str = "H++/vQgAAAAAAAAAc++/vSjvv70sLu+/vU0sVkjvv73vv70tKEotLlYoSS0uUSguKe+/ve+/vUsHANSP77+9Ix4AAAA="
//        let data = Data(base64Encoded: str, options: Data.Base64DecodingOptions(rawValue: 0))
//
//        let uncompressed = try! data!.gunzipped()
//
//        print("Data: \(uncompressed)")
    }
    
    func testUnencodeUnzip() {
        
        let original = "H++/vQgAAAAAAAAAZe+/ve+/vXLvv70wDEXvv73vv70KxJts77+9fkDvv71y77+9SO+/vUcy77+9eCZrSO+/vS3vv73vv73vv70hKWvvv73vv73vv73vv73vv71MM++/ve+/vQHvv73vv717AO+/vUPvv71yS++/ve+/veWGvnDvv70lXdGL77+977+9dErvv71dT++/vV7vv73vv73vv71r77+977+9bzxI77+9e++/vSTvv70f77+9Xe+/vVnvv73Pt++/ve+/vdOIJu+/vQfvv71C77+977+9c++/ve+/vTvvv73vv70c77+9Tu+/ve+/vV9LCl3vv71177+9d0ob77+977+9d1IPI++/ve+/ve+/vUHvv71177+9U++/vS7vv73vv70R77+9W++/vSIN77+9Fu+/ve+/vRB977+977+9Ie+/vUHvv73vv71laGMY77+977+977+9de+/ve+/vUxFayHvv71nGTvvv70uchjvv73vv71L0qEq77+9MGbvv70aMu+/vVHvv71TD0c0c++/ve+/vdOUG3fvv73Hr++/ve+/ve+/ve+/vUfvv73vv71OMe+/vU8877+9KO+/ve+/vTXvv73vv73vv718Cinvv70G77+92qzvv70K77+9YO+/ve+/ve+/vWVuNjUJWu+/vUfvv70P77+9Pe+/vTvvv73vv73vv73DkSEwC++/ve+/vR7vv73vv70r77+977+9xrjQge+/vWQBDFXvv73vv71S1Y/vv707cO+/vWvvv73vv73vv71b77+9XmHvv73vv73vv71NNe+/vXpwCFnSsnXvv71e77+9C++/vVfvv73epnfvv73vv711FN+477+9AUzvv73vv705IA9f77+9YyRR77+9EH8GWe+/vUAkdO+/vVzvv71Z77+9Ve+/vW4I1qnvv70YLyQ/77+9LUzvv73vv70S77+977+977+977+9y7Mg77+9x6jFqO+/ve+/ve+/ve+/vSBjyaQH77+9HO+/vVzvv73vv73vv73vv73vv70s77+9RiHvv73FgO+/ve+/vWXvv70+UO+/ve+/vRosP++/vWEg77+977+9Sy7vv73vv70z77+9WO+/vV5q77+9N15b77+9eSjvv71tECnvv70K77+977+9GO+/ve+/vS3vv71S77+9be+/vT/vv71l77+9KEtp77+9Gu+/ve+/vTEyED3vv70ZYh/vv70277+9XEht77+9Ue+/ve+/vW093aPvv73vv71g77+9aO+/ve+/vV7Xqtm2Fgvvv70/bO+/vX/vv70C7Ii977+9Yu+/ve+/vQ7vv71MRu+/ve+/ve+/vXpc77+977+9AO+/ve+/ve+/vSlf77+977+9Yk0977+9LVF2T2vvv705ZO+/vUPvv70uFu+/vUbvv711z6ojUh1WxaXvv73vv73vv71277+9bBnvv73vv73vv73vv70OUO+/vSTvv73vv71eK++/ve+/ve+/ve+/ve+/ve+/ve+/vSwpYu+/vUAP77+97JO7E++/ve+/vT3vv70p77+90azvv73gob1C77+977+977+9G++/vRfvv70y77+9OO+/vQQAAA=="
        
        let encoded = Data(original.utf8)
        let encodedData = Data(base64Encoded: encoded, options: Data.Base64DecodingOptions(rawValue: 0))

        let uncompressed = try! encodedData?.gunzipped()
        let uncompressedSentence = String(data: (uncompressed)!, encoding: .utf8)

        print(uncompressedSentence)
    }
}

