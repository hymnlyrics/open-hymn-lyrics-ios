//
//  Hymn_Lyrics_Tests.swift
//  Hymn Lyrics Tests
//
//  Created by Tseng, Oliver on 12/25/18.
//  Copyright © 2018 Ecodia. All rights reserved.
//

import XCTest
@testable import HymnLyrics

class HymnLyricsFree_Tests: XCTestCase {

    override func setUp() {
    }

    override func tearDown() {
    }

    func testForegroundColor() {
        var colors = "White/Black"
        var fg = Common.getForegroundColor(colors)
        XCTAssertTrue(fg == UIColor.white)
        
        colors = "Green/Black"
        fg = Common.getForegroundColor(colors)
        XCTAssertTrue(fg == UIColor.green)
    }

    func testBackgroundColor() {
        var colors = "White/Black"
        var fg = Common.getBackgroundColor(colors)
        XCTAssertTrue(fg == UIColor.black)
        
        colors = "Green/White"
        fg = Common.getBackgroundColor(colors)
        XCTAssertTrue(fg == UIColor.white)
    }

}
